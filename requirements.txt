Django==3.1.7
gunicorn==20.0.4
pytest==6.2.2
pytest-cov==2.11.1
pytest-django==4.1.0
